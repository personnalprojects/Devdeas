import React, { Component } from 'react';
import * as Icon from 'react-bootstrap-icons';
import Timestamp from 'react-timestamp';
import api_conf from '../api.config.js';
import '../sass/styles.scss';

class Dashboard extends Component{
  constructor(props){
    super(props);
    this.state = {
      ideas: [],
      certified_ideas: [],
      started: "N/A",
      db: ""
    };
  }

  getIdeas(){
    fetch(`${api_conf.API_URL}/all`)
    .then(res => res.json())
    .then(res => this.setState({
      ideas: res
    }));
  }

  getCertifiedIdeas(){
    fetch(`${api_conf.API_URL}/certified`)
    .then(res => res.json())
    .then(res => this.setState({
      certified_ideas: res
    }));
  }

  getConnection(){
    fetch(`${api_conf.API_URL}/status`)
    .then(res => res.json())
    .then(res => this.setState({
      db: res.devdeas_status
    }));
  }

  componentWillMount(){
    this.getIdeas();
    this.getCertifiedIdeas();
    // this.getUptime();
    this.getConnection();
  }

  render(){
    return(
      <div id="dashboard" className="mt-5 p-3 col-11 col-lg-7">
        <h2>Service dashboard</h2>

        <div className="d-flex flex-wrap justify-content-around align-items-center">
          <div className="text-center mt-3">
            <h4><Icon.CloudCheckFill/> Status :</h4>
            {
              this.state.db === "OK" &&
              <p>Running</p>
            }
            {
              this.state.db === null &&
              <p>Offline</p>
            }
            {
              this.state.db === "" &&
              <p>Offline</p>
            }
          </div>
          <div className="text-center mt-3">
            <h4><Icon.ClockHistory/> Uptime :</h4>
            {
              this.state.started === "N/A" &&
              <p>N/A</p>
            }
          </div>
          <div className="text-center mt-3">
            <h4><Icon.Braces/> Total ideas :</h4>
            {
              this.state.db === "OK" &&
              <p>{this.state.ideas.length}</p>
            }
            {
              this.state.db === null &&
              <p>Offline</p>
            }
            {
              this.state.db === "" &&
              <p>Offline</p>
            }
          </div>
          <div className="text-center mt-3">
            <h4><Icon.PatchCheckFll/> Certified ideas :</h4>
            {
              this.state.db === "OK" &&
              <p>{this.state.certified_ideas.length}</p>
            }
            {
              this.state.db === null &&
              <p>Offline</p>
            }
            {
              this.state.db === "" &&
              <p>Offline</p>
            }
          </div>
        </div>
      </div>
    )
  }
}

export default Dashboard;
